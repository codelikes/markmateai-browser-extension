# MarkMate AI Browser Extension

MarkMate AI, AI-powered Bookmark Manager: Organize, structure, and find your bookmarks effortlessly.

### Development Scripts

| Command                       | Description                                                     |
| ----------------------------- | --------------------------------------------------------------- |
| `npm run clean`               | Removes `build` and `release` directories.                      |
| `npm run build:dev`           | Builds the project in development mode.                         |
| `npm run build`               | Builds the project for production.                              |
| `npm run watch`               | Builds the project in development mode and watches for changes. |
| `npm run pack`                | Packs the extension.                                            |
| `npm run deploy`              | Builds and packs the extension, then deploys it.                |
| `npm run test`                | Runs all tests.                                                 |
| `npm run test:watch`          | Runs tests in watch mode.                                       |
| `npm run test:coverage`       | Generates a code coverage report.                               |
| `npm run prettier`            | Formats code using Prettier.                                    |
| `npm run lint`                | Checks code for errors with ESLint.                             |
| `npm run lint:fix`            | Automatically fixes some linting errors.                        |
| `npm run update-dependencies` | Checks and updates project dependencies.                        |

### Deploy Chrome Extension

#### How to generate Google API keys

[Follow the guide here](https://github.com/fregante/chrome-webstore-upload/blob/main/How%20to%20generate%20Google%20API%20keys.md)

### License

This project is open source and available under the MIT License.

---

Feel free to modify and use this script as needed for your projects. If you encounter any issues or have suggestions for improvements, please let us know.

### todo:

- https://github.com/GoogleChrome/chrome-extensions-samples примеры расширений от гугла
- https://developer.chrome.com/docs/webstore/publish/#publishing-to-test-accounts инструкция по публикации расширения
-
